# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field

class OspaItem(Item):
    # define the fields for your item here like:
    # name = Field()
    id = Field()
    nome_evento = Field()
    programa = Field()
    inicio = Field()
    local = Field()
    endereco = Field()
    ingressos = Field()
    data = Field()
    hora = Field()
