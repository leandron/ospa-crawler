# -*- coding: utf-8 -*-
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector
from scrapy.spider import Spider

from ospa.items import OspaItem

import re

class OspaSpider(CrawlSpider):
    name = "ospa"
    allowed_domains = ["ospa.org.br"]
    start_urls = [
        "http://www.ospa.org.br/?post_type=sp_events&eventDisplay=upcoming",
    ]

    rules = (
        Rule(SgmlLinkExtractor(restrict_xpaths=('//*[@id="tec-nav-below"]/div[2]/a')), follow=True, callback='parse_start_url'),
    )


    def get_month_number_from_string(self, month_string):
        # TODO: remove march workaround
        months = ['janeiro', 'fevereiro', 'mar', 'abril', 'maio', 'junho',
            'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro']

        if month_string.lower() in months:
            return months.index(month_string.lower()) + 1
        else:
            print month_string, months
            return None


    def extract_date_time(self, date_time_str):
        date_time_regex = re.compile(r'([ 123]*[0-9]) de (\w+)\s*(\d*:\d*)*')
        regex_match = date_time_regex.search(date_time_str)
        regex_groups = regex_match.groups()

        if len(regex_groups) == 3:
            # (dia, mes, hora:minuto)
            day_number = regex_groups[0]
            month_number = self.get_month_number_from_string(regex_groups[1])
            time = regex_groups[2] if regex_groups[2] else 'A definir'
            complete_date = '%s/%s' % (day_number, month_number)
            return complete_date, time
        else:
            return ('','')


    def parse_start_url(self, response):
        sel = Selector(response)

        # get all presentations of this page
        all_presentations = sel.xpath('//*[@id="tec-events-loop"]/div')

        presentation_items = []

        for presentation in all_presentations:
            this_presentation = OspaItem()
            this_presentation['inicio'] = ''
            this_presentation['local'] = ''
            this_presentation['endereco'] = ''
            this_presentation['ingressos'] = ''
            this_presentation['data'] = ''
            this_presentation['hora'] = ''

            id_extracted = presentation.xpath('./@id').extract()
            this_presentation['id'] = '' if len(id_extracted) != 1 else id_extracted[0]

            name_extracted = presentation.xpath('./h2/a/text()').extract()
            this_presentation['nome_evento'] = '' if len(name_extracted) != 1 else name_extracted[0]

            programa_extracted = presentation.xpath('./div[contains(@class, "entry-content")]/*').extract()
            this_presentation['programa'] = '' if len(programa_extracted) == 0 else ' '.join(programa_extracted)

            presentation_metadata_table = presentation.xpath('./div[2]/table/tr')

            for metadata_item in presentation_metadata_table:
                metadata_name_extracted = metadata_item.xpath('./td[@class="tec-event-meta-desc"]/text()').extract()
                metadata_value_extracted =  metadata_item.xpath('./td[@class="tec-event-meta-value"]/text()').extract()

                if len(metadata_name_extracted) > 0 and len(metadata_value_extracted) == 1:
                    metadata_name = metadata_name_extracted[0]   
                    metadata_value = metadata_value_extracted[0]

                    if u'In\xedcio:' in metadata_name:
                        curr_date, curr_time = self.extract_date_time(metadata_value)
                        this_presentation['inicio']  = metadata_value
                        this_presentation['data'] = curr_date
                        this_presentation['hora'] = curr_time

                    if u'Local:' in metadata_name:
                        this_presentation['local'] = metadata_value

                    if u'Endere\xe7o:' in metadata_name:
                        this_presentation['endereco'] = metadata_value

                    if u'Ingressos:' in metadata_name:
                        this_presentation['ingressos'] = metadata_value

            presentation_items.append(this_presentation)
        
        return presentation_items
