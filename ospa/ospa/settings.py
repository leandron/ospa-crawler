# Scrapy settings for ospa project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'ospa'

SPIDER_MODULES = ['ospa.spiders']
NEWSPIDER_MODULE = 'ospa.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'ospa (+http://www.yourdomain.com)'
